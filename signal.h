#ifndef SIGNAL_H
#define SIGNAL_H

#define SET(byte_reg, val) byte_reg |= (1<<val);

#define CLEAR(byte_reg, val) byte_reg &= ~(1<<val);

#define ISSET(byte_reg, val) (byte_reg & (1<<val))

// untuk menghandle event async
uint8_t signal;

#define SIG_APPL 0
#define SIG_TFIN 1
#define SIG_TNOT 2
#define SIG_TIQO 3
#define SIG_TOFF 4
#define SIG_DETI 5
#define SIG_SHOF 6
#define SIG_MATC 7

#endif
