#ifndef TICKER_H
#define TICKER_H

/* TODO
 *
 * Implementasi viewport
 *
 * */

class MyTicker
{
    public:
        void init(uint8_t x, uint8_t y, uint8_t l, uint8_t r);
        void step();
        void set_text(char* t);
        void run(uint8_t n=0);
        void stop();
        void reset();
        bool is_running();
        void repeat();

    private:
        // pointer to text buffer
        char* text;
        uint8_t text_len;

        // jumlah karakter yang sudah out of screen
        uint8_t offset;

        // status
        bool is_run;

        // posisi Y pada display
        int pos_x;
        int pos_y;

        // viewport
        uint8_t left;
        uint8_t right;

        // pengulangan text
        char repeat_count;
};

#endif
