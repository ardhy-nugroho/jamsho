#ifndef CONFIG_H
#define CONFIG_H

#include <avr/eeprom.h>

// DOT MATRIX
// pilih salah satu
//#define CONFIG_ONE_PANEL
#define CONFIG_THREE_PANEL

#define CONFIG_RTC

#define CONFIG_DEBUG

#define CONFIG_IQOMAH
#define CONFIG_TICKER
#define CONFIG_SMART_REMINDER

/*
 * Configuration data structure
 * */
typedef struct
{
    // lokasi
    int32_t lat;
    int32_t lon;
    int32_t alt;
    int16_t tz;
    // sudut hisab
    uint16_t asub;
    uint16_t aset;
    uint16_t asya;
    uint8_t asar;
    // penyesuaian menit
    uint8_t msub;
    uint8_t mdhu;
    uint8_t mzhu;
    uint8_t masr;
    uint8_t mmag;
    uint8_t misy;
    // timer
    uint8_t tfin;
    uint8_t tnot;
    uint8_t tiqo;
    uint8_t toff;
    uint8_t tiqs;
    uint8_t tiqz;
    uint8_t tiqa;
    uint8_t tiqm;
    uint8_t tiqi;
    // device
    uint8_t brit;
    // adjustment
    char hijri;
    // high latitude
    uint8_t hilat;
} s_config;

union u_runtime
{
    s_config config;
    byte bytes[36];
} runtime;

s_config rom_config EEMEM;

// default value
// beberapa variabel yang bertipe floating point disimpan
// dalam bentuk x1000
const s_config default_config PROGMEM =
{
    -6174,
    106850,
    50000,
    7000,
    17000,
    833,
    18000,
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    15,
    5,
    10,
    15,
    // iqomah spesifik waktu sholat
    0,
    0,
    0,
    0,
    0,
    5,
    0,
    0
};

#endif
