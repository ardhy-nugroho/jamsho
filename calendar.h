// define header protection
// to aviod multiple inclusion
#ifndef CALENDAR_H
#define CALENDAR_H

#define CAL_GREGORIAN_EPOCH 1721425.5
#define CAL_ISLAMIC_EPOCH 1948439.5

#define MUHARRAM    1
#define SAFAR       2
#define RAB1        3
#define RAB2        4
#define JUM1        5
#define JUM2        6
#define RAJAB       7
#define SYABAN      8
#define RAMADHAN    9
#define SYAWWAL     10
#define DZULQODAH   11
#define DZULHIJJAH  12

#define AHAD 0
#define SENIN 1
#define SELASA 2
#define RABU 3
#define KAMIS 4
#define JUMAT 5
#define SABTU 6

// use extern to share global variable in .c file
// to other source code
#ifndef __AVR__
/// Nama-nama hari islam
const char cal_islamic_weekdays[7][20] =
{
    "al-ahad",
    "al-ithnayn",
    "ath-thalatha",
    "al-arba'",
    "al-khamis",
    "al-juma",
    "as-sabt"
};
#else
#include <avr/pgmspace.h>

const char cal_islamic_weekdays[14][7] PROGMEM =
{
    {"Ahad"}, {"Senin"}, {"Selasa"}, {"Rabu"}, {"Kamis"}, {"Jumat"}, {"Sabtu"},
    {"Ahd"}, {"Sen"}, {"Sel"}, {"Rab"}, {"Kam"}, {"Jum"}, {"Sab"}
};

const char cal_islamic_months[24][14] PROGMEM =
{
    {"Muh"}, {"Saf"}, {"Rab1"}, {"Rab2"}, {"Jum1"}, {"Jum2"},
    {"Raj"}, {"Syab"}, {"Ram"}, {"Syaw"}, {"Dzqh"}, {"Dzhj"},
    {"Muharram"}, {"Safar"}, {"Rabiul-Awwal"}, {"Rabiul-Akhir"}, {"Jumadil-Awwal"}, {"Jumadil-Akhir"},
    {"Rajab"}, {"Syaban"}, {"Ramadhan"}, {"Syawwal"}, {"Dzul-Qodah"}, {"Dzul-Hijjah"}
};

const char cal_gregorian_months[24][10] PROGMEM =
{
    {"JAN"}, {"FEB"}, {"MAR"}, {"APR"}, {"MEI"}, {"JUN"},
    {"JUL"}, {"AGU"}, {"SEP"}, {"OKT"}, {"NOV"}, {"DES"},
    {"Januari"}, {"Februari"}, {"Maret"}, {"April"}, {"Mei"}, {"Juni"},
    {"Juli"}, {"Agustus"}, {"September"}, {"Oktober"}, {"November"}, {"Desember"}
};


#endif

// use prefix for easy identification
char cal_leap_gregorian(
        unsigned int year
        );

double cal_gregorian_to_jd(
        unsigned int year,
        unsigned char month,
        unsigned char day
        );

void cal_jd_to_gregorian(
        double jd,
        unsigned int &year,
        unsigned char &month,
        unsigned char &day
        );

double cal_islamic_to_jd(
        unsigned int year,
        unsigned char month,
        unsigned char day
        );

void cal_jd_to_islamic(
        double jd,
        unsigned int &year,
        unsigned char &month,
        unsigned char &day
);

#endif
