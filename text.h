#ifndef TEXT_H
#define TEXT_H

#include <avr/eeprom.h>

char rom_text[256] EEMEM;

uint8_t text_id;
#define TXT_COUNT 5
#define TXT_IDENTITAS 0
#define TXT_JADWAL 1
#define TXT_JELANG_AKHIR 2
#define TXT_ADAB_MASJID 3
#define TXT_HADITS 4

//// IDENTITAS PEMILIK

#define TXT_KEY 'a'

const char txt_identitas[] PROGMEM =
"AHLAN WA SAHLAN FI MASJID AS-SALAM * XL AXIATA TOWER";

#define TXT_CHIPER_LEN 52

const char txt_chipered[] PROGMEM =
{32,41,45,32,47,65,54,32,65,50,32,41,45,32,47,65,39,40,65,44,32,50,43,40,37,65,32,50,76,50,32,45,32,44,65,75,65,57,45,65,32,57,40,32,53,32,65,53,46,54,36,51};

/* *****************************
 * HADITS TERKAIT ADAB DI MASJID
 * *****************************/

#define TXT_ADAB_COUNT 5
uint8_t txt_adab_idx;

const char txt_shof_pertama[] PROGMEM =
"Seandainya manusia mengetahui keutamaan shaf pertama, dan tidaklah mereka bisa mendapatinya kecuali dengan berundi niscaya mereka akan berundi... -- HR. Bukhari no.615";

const char txt_segera_ke_masjid[] PROGMEM=
"...Dan seandainya mereka mengetahui keutamaan bersegera menuju masjid niscaya mereka akan berlomba-lomba -- HR. Bukhari no.615";

const char txt_tahiyatul_masjid[] PROGMEM =
"Jika salah seorang dari kalian masuk masjid, maka hendaklah dia shalat dua rakaat sebelum dia duduk -- HR. Bukhari no.537";

const char txt_hatinya_di_masjid[] PROGMEM =
"Salah satu dari tujuh jenis orang yang Allah Ta'ala akan menaungi mereka pada hari kiamat, yaitu laki-laki yang hatinya selalu terikat dengan masjid";

const char txt_dilarang_mengganggu[] PROGMEM =
//"Ketahuilah, kalian semua sedang bermunajat kepada Allah, maka janganlah saling mengganggu satu sama lain. Janganlah kalian mengeraskan suara dalam membaca Al-Quran";
"Rasulullah Sholallahu'alaihi Wasallam bersabda: Ketahuilah, kalian semua sedang bermunajat kepada Allah, maka janganlah saling mengganggu satu sama lain. Janganlah kalian mengeraskan suara dalam membaca Al-Quran. -- HR. Ahmad no.430";

/* ***************************************
 * HADITS PILIHAN SEPUTAR SHOLAT DAN PUASA
 * ***************************************/

uint8_t txt_hadits_idx;

const char txt_hadits_default[] PROGMEM =
"Sesungguhnya shalat itu adalah fardhu yang ditentukan waktunya atas orang-orang yang beriman -- QS. An-Nisa:103";

const char txt_berdiam_setelah_subuh[] PROGMEM =
//"BARANG SIAPA MENGERJAKAN SHALAT SUBUH BERJAMAAH DI MASJID LALU MENETAP BERDZIKIR DI MASJID SAMPAI MELAKSANAKAN SHOLAT DHUHA, BAGINYA PAHALA SEPERTI PAHALA HAJI DAN UMROH YANG SEMPURNA";
"Barang siapa mengerjakan shalat Subuh berjamaah di masjid lalu menetap dan berdzikir sampai melaksanakan sholat Dhuha, baginya pahala seperti pahala haji dan umroh, sempurna sempurna sempurna";

const char txt_reminder_dhuha[] PROGMEM =
"Wahai anak Adam, ruku'lah untuk-Ku empat raka'at di awal siang, niscaya Aku akan mencukupimu di akhir siang -- HR. At-Tirmidzi no.478";
/* [3]. Hadits hasan. Diriwayatkan oleh Ahmad di dalam kitab, Al-Musnad (VI/440 dan 451). Dan juga diriwayatkan oleh At-Tirmidzi di dalam Kitaabush Shalaah, bab Maa Jaa-a fii Shalaatidh Dhuha, (hadits no. 475) */
/* Mengenai hadits ini, At-Tirmidzi mengatakan : ‘Hasan gharib” Dan dinilai shahih oleh Syaikh Ahmad Syakir di dalam tahqiqnya pada At-Tirmidzi. Juga dinilai shahih oleh Al-Albani di dalam kitab, Shahih Sunan At-Tirmidzi, (I/147). Serta dinilai hasan oleh muhaqqiq kitab, Jaami’ul Ushuul (IX/4370). */
/* Sumber: https://almanhaj.or.id/2357-shalat-dhuha.html) */

const char txt_reminder_puasa_senin_kamis[] PROGMEM =
//"Amal-amal manusia diperiksa pada setiap hari Senin dan Kamis, maka aku menyukai amal perbuatanku diperiksa sedangkan aku dalam keadaan berpuasa";
/* [6]. Hadits ini diriwayatkan oleh at-Tirmidzi dalam Sunannya (III/122) Kitaabus Shaum bab Maa Jaa’a fii Shaum Yaumil Itsnain wal Khamiis dari Abu Hura-irah. At-Tirmidzi mengatakan, “Hadits ini Hasan Gharib.” Namun menurut Abu Dawud hadits ini memiliki syahid (penguat). Lihat Sunan Abi Dawud Ma’a Badzlil Majhuud (XI/304) Kitabush Shaum bab Shaum Yaumil Itsnain wal Khamiis. */
/* Sumber: https://almanhaj.or.id/3318-beberapa-keutamaan-dan-keberkahan-hari-senin-dan-kamis.html */
"Rasulullah Shallallahu'alaihi wa sallam sangat antusias dan bersungguh-sungguh dalam melakukan puasa pada hari Senin dan Kamis -- HR. At-Tirmidzi no.121";
/* Sumber: https://almanhaj.or.id/3318-beberapa-keutamaan-dan-keberkahan-hari-senin-dan-kamis.html */

const char txt_puasa_ayyamulbiidh[] PROGMEM =
"Rasulullah shallallahu'alaihi wa sallam biasa berpuasa pada ayyamul biidh (hari ke-13, 14, 15 dari bulan hijriyah) ketika tidak bepergian maupun ketika bersafar -- HR. An-Nasai no. 2345";
/*https://rumaysho.com/1127-8-macam-puasa-sunnah.html*/

const char txt_puasa_muharram[] PROGMEM =
"Puasa yang paling utama setelah (puasa) Ramadhan adalah puasa pada bulan Allah (tanggal 9, 10, 11 Muharram). Sementara shalat yang paling utama setelah shalat wajib adalah shalat malam -- HR. Muslim no. 1163";

const char txt_puasa_ramadhan[] PROGMEM =
"Wahai orang-orang yang beriman, diwajibkan atas kamu berpuasa sebagaimana diwajibkan atas orang-orang sebelum kamu agar kamu bertaqwa -- QS. Al-Baqarah:183";
/* [Al-Baqarah : 183] */

const char txt_puasa_syawal[] PROGMEM =
"Barangsiapa yang berpuasa Ramadhan kemudian berpuasa enam hari di bulan Syawal, maka dia seperti berpuasa setahun penuh -- HR. Muslim no. 1164";

const char txt_puasa_dzulhijjah[] PROGMEM =
"Amal shalih di 10 hari bulan Dzulhijjah lebih dicintai oleh Allah melebihi jihad fi sabilillah, kecuali orang yang keluar (berjihad) dengan jiwa dan hartanya, lalu tidak kembali dengan sesuatu apapun.";
/* Ditulis ulang. Sumber: https://almanhaj.or.id/2888-keutamaan-10-hari-pertama-bulan-dzulhijjah-dan-amalan-yang-disyariatkan.html */

/* ******************************************
 * ANTARA ADZAN & IQOMAH
 * Khusus waktu antara adzan dan iqomah
 * ******************************************/

#define TXT_IQO_COUNT 4

const char txt_keutamaan_sholat_sunnah_rawatib[] PROGMEM =
"Tidak ada seorang hamba muslim yang shalat sunnah 12 rakaat setiap hari karena Allah, kecuali akan dibangunkan baginya istana atau rumah di surga";

const char txt_sebelum_subuh[] PROGMEM =
"Dua rakaat sunnah fajr (sebelum Subuh) lebih baik daripada dunia dan seisinya";

const char txt_sholat_sunnah_rawatib[] PROGMEM =
"Sholat sunnah rawatib 12 rakaat, yaitu: 2 rakaat sebelum Subuh, 4 sebelum Zhuhur, 2 setelah Zhuhur, 2 setelah Maghrib dan 2 setelah Isya";

const char txt_doa_azan_iqomah[] PROGMEM =
"Sesungguhnya do'a yang tidak tertolak adalah do'a antara adzan dan iqomah, maka berdo'alah (kala itu) -- HR. Ahmad";

const char txt_sholat_antara_azan_iqomah[] PROGMEM =
"Antara adzan dan iqomah itu terdapat shalat (Rasul mengulanginya tiga kali) bagi siapa yang berkehendak -- HR. Bukhari";

#endif
