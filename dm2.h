/* vim: set sw=4 ts=4 sts=4 tw=78 foldmarker={,} foldlevel=0 foldmethod=marker nospell: */

/**
 * Dot Matrix 2 -- A light weight dot matrix library.
 *
 * This is main header file.
 *
 * This library inspired by DMD2 library by Freetronics.
 *
 * 2017/1438H Ardhy W. Nugroho
 *
 * */

#ifndef DM2_H
#define DM2_H

#include "Arduino.h"
#include <SPI.h>
/* #include "fonts/Arial14.h" */
/* #include "fonts/Droid_Sans_12.h" */
/* #include "fonts/Arial_Black_16.h" */
/* #include "fonts/Droid_Sans_16.h" */
/* #include "fonts/SystemFont5x7.h" */
/* #include "fonts/f8x11.h" */
/* #include "fonts/f8x8.h" */

//{## basic dot matrix
#define DM_PIN_A        7   // PD7 = D7
#define DM_PIN_B        6   // PD6 = D6
#define DM_PIN_RCLK     0   // PB0 = D8, register latch clock
#define DM_PIN_NOE      1   // PB1 = D9, output enabled (active low)
#define DM_PIN_SER      3   // PB3 = D11 = MOSI, serial data
#define DM_PIN_SRCLK    5   // PB5 = D13 = SCK, serial clock
#define DM_PIN_SS       2   // PB2 = D10
#define DM_PIN_PWM      9   // arduino pin number, ini adalah pin nOE

enum graphics_mode {
    GRAPHICS_OFF,
    GRAPHICS_ON,
    GRAPHICS_INVERSE,
    GRAPHICS_OR,
    GRAPHICS_NOR,
    GRAPHICS_XOR,
    GRAPHICS_NOP
};

struct FontHeader
{// Six byte font header at beginning of FontCreator font structure
    unsigned int size;
    unsigned char fixed_width;
    unsigned char height;
    unsigned char first_char;
    unsigned char char_count;
};

extern FontHeader font_header;
extern uint8_t font_padding;

const unsigned char bitmask[8] =
{// lookup table untuk bitmap
    0x80,
    0x40,
    0x20,
    0x10,
    0x08,
    0x04,
    0x02,
    0x01
};

extern unsigned char dm_height;
extern unsigned char dm_width;

void dm_init(unsigned char w, unsigned char h);
void dm_clear(graphics_mode mode=GRAPHICS_ON);
void set_cursor(int x, int y);
unsigned char is_on_screen(int x, int y);
void dm_pixel(int x, int y, graphics_mode mode=GRAPHICS_ON);
void dm_scan();
void dm_brightness(unsigned char b);
void dm_line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode=GRAPHICS_ON);
void dm_box(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode=GRAPHICS_ON);
void dm_fbox(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode=GRAPHICS_ON);
void select_font(const unsigned char* f);
uint8_t char_width(char letter);
uint8_t string_width(const char* s);
uint8_t dm_char(int x, int y, char c, graphics_mode mode=GRAPHICS_ON);
uint8_t dm_string(int x, int y, const char* s, graphics_mode mode=GRAPHICS_ON);
void dm_viewport(int x1=0, int y1=0, int x2=0, int y2=0);
//##}

//{## animation

enum data_mode
{
    RAM, // RAM
    PGM, // progmem
    EEM  // eeprom memory
};

struct Ticker
{// ticker structure data used for rendering the ticker
    const char* txt; // should point to char buffer
    int x; // ticker x position
    unsigned int offset; // this is actually to track number of chars that already out of viewport
    unsigned int len; // save ticker text len, to minimize repetitive use of strlen(), to increase performance
    uint8_t left; // left view port
    uint8_t right; // right view port
    bool enabled;
    data_mode mode;
};

Ticker dm_ticker_text(const char* s, int right, int left, data_mode mode);
void dm_ticker(int x, int y, Ticker* ticker);
void enable_ticker(Ticker* t);
void reset_ticker(Ticker* t);
//##}

//{## user interface
void dm_date(uint8_t x, uint8_t y);
uint8_t dm_clock(int x, int y, uint8_t d1, uint8_t d2, bool blink=true, bool clear=false);
void dm_count_down(int x, int y, uint16_t &sec);
void dm_jadwal(int x, int y);
//##}

//{## text
void txt_center(const char *txt, int y=0);
void txt_right(const char *txt, int y=0);
//##}
#endif
