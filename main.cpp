/* vim: set sw=4 ts=4 sts=4 tw=78 foldmarker={,} foldlevel=0 foldmethod=marker nospell: */

/*
 * varian
 *
 * ## defaut, all varian:
 *  . SW menghitung waktu sholat berdasar lokasi dan altitude
 *  . SW parameter sudut: subuh; sunrise/sunset; isya
 *  . SW parameter bayangan asar: 1x; 2x.
 *  . SW parameter pengaturan menit: sholat 5 waktu; dhuha
 *  . SW tanggal hijriyah
 *  . SW serial command
 *  . SW adjustable timer
 *  . HW brightness adjustment
 *  . HW serial connection
 *  . HW buzzer
 *  - HW remote control untuk simple control: start iqomah count; blank screen
 *
 * ## mini/basic untuk rumah:
 *  . DSP jam
 *  . DSP nama waktu
 *  . HW 1 panel P10
 *  . ALM masuk waktu sholat (5x beep)
 *  . ALM sebelum habis waktu sholat (1x beep; blink)
 *
 * ## mini/basic untuk masjid:
 *  . TMR jeda untuk adzan (configurable)
 *  . TMR count down iqomah (configurable)
 *  . TMR blank screen selama sholat (configurable)
 *  . DSP count down iqomah
 *
 * ## regular running text
 *  - HW 3x panel P10
 *  - TXT reminder sholat dhuha; sholat rawatib
 *  - TXT reminder berdiam diri setelah subuh
 *  - TXT reminder puasa senin kamis
 *  - DSP running text
 * */

#include "Arduino.h"
#include <Wire.h>
#include <RTClib.h>
#include "SerialCommand.h"
#include "config.h"
#include "text.h"
#include "wsholat.h"
#include "calendar.h"
#include "astro.h"
#include "signal.h"
#include "dm2.h"
#include "fonts/f8x8.h"
#include "ticker.h"

#define PIN_BEEP 2 // port C 2

#define SHOLAT_5_WAKTU (\
        (current_time == SUBUH)||\
        (current_time == ZHUHUR)||\
        (current_time == ASAR)||\
        (current_time == MAGHRIB)||\
        (current_time == ISYA))

#define SHOLAT_DHUHA (current_time == DHUHA)

#define SHOLAT_ASAR (current_time == ASAR)

#define LAGI_PUASA (\
        ((hijriyah.m == MUHARRAM)&&(hijriyah.d == 11 || hijriyah.d == 10 || hijriyah.d == 9)) ||\
        (hijriyah.m == RAMADHAN)||\
        (hijriyah.m == SYAWWAL)||\
        ((hijriyah.m == DZULHIJJAH)&&(hijriyah.d < 10)))

#define AYYAMULBIIDH (\
        hijriyah.d == 13 ||\
        hijriyah.d == 14 ||\
        hijriyah.d == 15)

#define SENEN_KAMIS (\
        now.dayOfTheWeek() == SENIN ||\
        now.dayOfTheWeek() == KAMIS)

#define BULAN_RAMADHAN (hijriyah.m == RAMADHAN)

RTC_DS3231 rtc;
DateTime now;
SerialCommand scmd;
WaktuSholat sholat;

#ifdef CONFIG_TICKER
MyTicker the_ticker;
#endif

typedef struct
{
    uint16_t y;
    uint8_t m;
    uint8_t d;
} s_date;

s_date hijriyah;

// timer tunggu menuju waktu berikut
uint32_t timer;
//
#ifdef CONFIG_IQOMAH
uint16_t wait_timer;
#endif

// floating point formated time
double floating_time;

// tracking waktu
uint8_t current_time;
uint8_t next_time;

// timer untuk beep
uint8_t beep_timer;

// text buffer
char buf[80];

#ifdef CONFIG_THREE_PANEL
char ticker_text[256];
#endif

// flag if buzzer is beeping
bool is_beeping;

volatile bool f_ticker;

//{## KALENDER HIJRIYAH
void hijri()
{
    // konversi gregorian to jd
    double jd = cal_gregorian_to_jd(now.year(), now.month(), now.day());

    // adjust apakah maju atau mundur
    jd += runtime.config.hijri;

    // jika ketika dipanggil sudah lewat maghrib, maka ditambah 1
    // karena pergantian hari di Islam adalah maghrib
    if((now.hour()*100 + now.minute()) >= floor(sholat.waktu[MAGHRIB] * 100) )
    {
        jd += 1;
    }

    cal_jd_to_islamic(jd, hijriyah.y, hijriyah.m, hijriyah.d);
}
//}

//{## COMMAND HANDLER

void cmd_sho()
{
    // Runtime configuration
    sprintf_P(buf, PSTR("Location:\r\n lat=%ld\r\n lon=%ld\r\n alt=%ld\r\n tz=%d\r\n hil=%d"), runtime.config.lat, runtime.config.lon, runtime.config.alt, runtime.config.tz, runtime.config.hilat);
    Serial.println(buf);

    sprintf_P(buf, PSTR("Angle:\n\r sub=%d\r\n ter=%d\r\n asa=%d\r\n isy=%d"), runtime.config.asub, runtime.config.aset, runtime.config.asar, runtime.config.asya);
    Serial.println(buf);

    sprintf_P(buf, PSTR("Adjustment:\r\n ms=%d\r\n md=%d\r\n mz=%d\r\n ma=%d\r\n mm=%d\r\n mi=%d\r\n hij=%d"), runtime.config.msub, runtime.config.mdhu, runtime.config.mzhu, runtime.config.masr, runtime.config.mmag, runtime.config.misy, runtime.config.hijri);
    Serial.println(buf);

    sprintf_P(buf, PSTR("Timer:\n\r akh=%d\r\n aza=%d\r\n iqo=%d\r\n off=%d"), runtime.config.tfin, runtime.config.tnot, runtime.config.tiqo, runtime.config.toff);
    Serial.println(buf);

    sprintf_P(buf, PSTR("Iqomah:\n\r iqs=%d\r\n iqz=%d\r\n iqa=%d\r\n iqm=%d\r\n iqi=%d"), runtime.config.tiqs, runtime.config.tiqz, runtime.config.tiqa, runtime.config.tiqm, runtime.config.tiqi);
    Serial.println(buf);

    sprintf_P(buf, PSTR("Display:\n\r bri=%d"), runtime.config.brit);
    Serial.println(buf);

#ifdef CONFIG_DEBUG
    strcpy_P(buf, txt_identitas);
    uint8_t l = strlen(buf);
    Serial.println(l);
    for(l=0; l<strlen(buf); l++)
    {
        uint8_t x = buf[l] ^ TXT_KEY;
        Serial.print(x);
        Serial.print(',');
    }
    Serial.println();
#endif

    // Custom Text
    Serial.println("\nROM text:");
    for(uint16_t i=0; i<sizeof(rom_text); i++)
    {
        char c = eeprom_read_byte((const uint8_t*)&rom_text+i);

        if(c==0)
            break;

        Serial.print(c);
    }
    Serial.println("\n");
#ifdef CONFIG_RTC
    // Clock
    hijri();
    sprintf_P(buf, PSTR("\nTime:\r\n %d-%02d-%02d %02d:%02d:%02d\r\n hijri=%d-%02d-%02d\n"), now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second(), hijriyah.y, hijriyah.m, hijriyah.d);
    Serial.println(buf);

    for(uint8_t i = 0; i < JUMLAH_WAKTU; i++)
    {
         uint8_t h = floor(sholat.waktu[i]);
         uint8_t m = (sholat.waktu[i] - h) * 60;
         sprintf_P(buf, PSTR("%02d:%02d "), h, m);
         strcat_P(buf, nama_waktu[i]);
         if(i == current_time)
             strcat_P(buf, PSTR(" *"));
         Serial.println(buf);
    }
#endif
}

void cmd_set()
{
    char *arg = scmd.next();
    char *value = scmd.next();
    int32_t val = atol(value);

    // RTC
    if(!strcmp_P(arg, PSTR("rtc")))
    {
        uint8_t bulan = atoi(scmd.next());
        uint8_t hari = atoi(scmd.next());
        uint8_t jam = atoi(scmd.next());
        uint8_t menit = atoi(scmd.next());

#ifdef CONFIG_DEBUG
        sprintf_P(buf, PSTR("receive command rtc() set %d-%02d-%02d %02d:%02d"), val, bulan, hari, jam, menit);
        Serial.println(buf);
#endif
        now = DateTime(val, bulan, hari, jam, menit);
        rtc.adjust(now);
    }
    // txt
    else if(!strcmp_P(arg, PSTR("txt")))
    {
        if(!value)
        {
            uint16_t i;
            for(i=0; i<sizeof(rom_text); i++)
            {
                char c = eeprom_read_byte((const uint8_t*)&rom_text+i);

                if(c==0)
                    break;

            }
            // return string length
            Serial.println(i);
        }
        else
        {
            char c = scmd.next()[0];
            uint16_t addr = (uint16_t)rom_text;
            addr = addr + val;

            if(!c)
                 Serial.println((char)eeprom_read_byte((uint8_t *)addr));
            else
            {
                // ESC == space
                if(c == 27)
                    c = 32;

                eeprom_update_byte((uint8_t *)addr, c);
            }

        }
    }
    //// LOKASI
    // latitude
    else if(!strcmp_P(arg, PSTR("lat")))
    {
        if(!value)
            Serial.println(runtime.config.lat);
        else
            runtime.config.lat = val;
    }
    // longitude
    else if(!strcmp_P(arg, PSTR("lon")))
    {
        if(!value)
            Serial.println(runtime.config.lon);
        else
            runtime.config.lon = val;
    }
    // altitude
    else if(!strcmp_P(arg, PSTR("alt")))
    {
        if(!value)
            Serial.println(runtime.config.alt);
        else
            runtime.config.alt = val;
    }
    // timezone
    else if(!strcmp_P(arg, PSTR("tz")))
    {
        if(!value)
            Serial.println(runtime.config.tz);
        else
            runtime.config.tz = val;
    }
    //// PARAMETER HISAB
    // sudut subuh
    else if(!strcmp_P(arg, PSTR("sub")))
    {
         if(!value)
             Serial.println(runtime.config.asub);
         else
            runtime.config.asub = val;
    }
    // sunrise/sunset
    else if(!strcmp_P(arg, PSTR("ter")))
    {
        if(!value)
            Serial.println(runtime.config.aset);
        else
            runtime.config.aset = val;
    }
    // sudut isya
    else if(!strcmp_P(arg, PSTR("isy")))
    {
        if(!value)
            Serial.println(runtime.config.asya);
        else
            runtime.config.asya = val;
    }
    // bayangan asar
    else if(!strcmp_P(arg, PSTR("asa")))
    {
        if(!value)
            Serial.println(runtime.config.asar);
        else
            runtime.config.asar = val;
    }
    // PENYESUAIAN MENIT
    // menit subuh
    else if(!strcmp_P(arg, PSTR("ms")))
    {
        if(!value)
            Serial.println(runtime.config.msub);
        else
            runtime.config.msub = val;
    }
    // menit dhuha
    else if(!strcmp_P(arg, PSTR("md")))
    {
        if(!value)
            Serial.println(runtime.config.mdhu);
        else
            runtime.config.mdhu = val;
    }
    // menit zhuhur
    else if(!strcmp_P(arg, PSTR("mz")))
    {
        if(!value)
            Serial.println(runtime.config.mzhu);
        else
            runtime.config.mzhu = val;
    }
    // menit asar
    else if(!strcmp_P(arg, PSTR("ma")))
    {
        if(!value)
            Serial.println(runtime.config.masr);
        else
            runtime.config.masr = val;
    }
    // menit maghrib
    else if(!strcmp_P(arg, PSTR("mm")))
    {
        if(!value)
            Serial.println(runtime.config.mmag);
        else
            runtime.config.mmag = val;
    }
    // menit isya
    else if(!strcmp_P(arg, PSTR("mi")))
    {
        if(!value)
            Serial.println(runtime.config.misy);
        else
            runtime.config.misy = val;
    }
    //// TIMER
    // peringatan sebelum waktu berakhir
    else if(!strcmp_P(arg, PSTR("akh")))
    {
        if(!value)
            Serial.println(runtime.config.tfin);
        else
            runtime.config.tfin = val;
    }
    // notifikasi -- lamanya blinking sebelum countdown iqomah
    else if(!strcmp_P(arg, PSTR("aza")))
    {
        if(!value)
            Serial.println(runtime.config.tnot);
        else
            runtime.config.tnot = val;
    }
    // iqomah
    else if(!strcmp_P(arg, PSTR("iqo")))
    {
        if(!value)
            Serial.println(runtime.config.tiqo);
        else
            runtime.config.tiqo = val;
    }
    else if(!strcmp_P(arg, PSTR("iqs")))
    {
        if(!value)
            Serial.println(runtime.config.tiqs);
        else
            runtime.config.tiqs = val;
    }
    else if(!strcmp_P(arg, PSTR("iqz")))
    {
        if(!value)
            Serial.println(runtime.config.tiqz);
        else
            runtime.config.tiqz = val;
    }
    else if(!strcmp_P(arg, PSTR("iqa")))
    {
        if(!value)
            Serial.println(runtime.config.tiqa);
        else
            runtime.config.tiqa = val;
    }
    else if(!strcmp_P(arg, PSTR("iqm")))
    {
        if(!value)
            Serial.println(runtime.config.tiqm);
        else
            runtime.config.tiqm = val;
    }
    else if(!strcmp_P(arg, PSTR("iqi")))
    {
        if(!value)
            Serial.println(runtime.config.tiqi);
        else
            runtime.config.tiqi = val;
    }
    // blank/off screen ketika sholat didirikan
    else if(!strcmp_P(arg, PSTR("off")))
    {
        if(!value)
            Serial.println(runtime.config.toff);
        else
            runtime.config.toff = val;
    }
    //// HARDWARE
    // display brightness
    else if(!strcmp_P(arg, PSTR("bri")))
    {
        if(!value)
            Serial.println(runtime.config.brit);
        else
        {
            runtime.config.brit = val;
            dm_brightness(val);
        }
    }
    // hijri
    else if(!strcmp_P(arg, PSTR("hij")))
    {
        if(!value)
            Serial.println(runtime.config.hijri);
        else
        {
            runtime.config.hijri = val;
            hijri();
        }
    }
    // hi-latitude
    else if(!strcmp_P(arg, PSTR("hil")))
    {
        if(!value)
            Serial.println(runtime.config.hilat);
        else
            runtime.config.hilat = val;
    }
    // save configuration permanently in eeprom
    else if(!strcmp_P(arg, PSTR("sav")))
    {
        eeprom_update_block(&runtime.config, &rom_config, sizeof(s_config));
    }
    // reset to default value
    else if(!strcmp_P(arg, PSTR("rst")))
    {
        memcpy_P(&runtime.config, &default_config, sizeof(s_config));

        // default text
        char default_text[40] = "** Custom text up to 256 characters **\0";
        eeprom_update_block(&default_text, &rom_text, sizeof(default_text));
    }

    else if(!strcmp_P(arg, PSTR("ok")))
    {
        SET(signal, SIG_APPL);
    }
    else
    {
        strcpy_P(buf, PSTR("Nusays Jamshoes (Jam sholat embedded software), 1438H"));
        Serial.println(buf);
        sprintf_P(buf, PSTR("Firmware build for: Musholla Al-Muhajirin, Tangerang, 1439H (%s)"), __DATE__);
        Serial.println(buf);
    }

}

void cmd_beep()
{// test buzzer
    beep_timer = 1;
}
//}

void setup()
{// SETUP
    // SERIAL SETUP
    Serial.begin(57600);

    // SERIAL COMMAND REGISTRATION
    // Agak buggy kalau addCommand dilakukan setelah init Ticker
    // Better ditaruh disini
    scmd.addCommand("g", cmd_sho);
    scmd.addCommand("v", cmd_set);
    scmd.addCommand("b", cmd_beep);

    // LOADING CONFIGURATION
    eeprom_read_block(&runtime.config, &rom_config, sizeof(s_config));

#ifdef CONFIG_RTC
    // RTC SETUP
    Wire.begin();
    rtc.begin();
#endif

#ifdef CONFIG_TICKER
    // TIMER SETUP
    TCCR2A = 0;
    TCNT2 = 130;
    TCCR2B = (_BV(CS22)|_BV(CS20));
    TIMSK2 = _BV(TOIE2);
#endif

    // DOT MATRIX
#ifdef CONFIG_ONE_PANEL
    dm_init(32, 16);
#endif

#ifdef CONFIG_THREE_PANEL
    dm_init(96, 16);
#endif

    dm_brightness(runtime.config.brit);
    dm_clear();
    dm_viewport();
    select_font(f8x8);

#ifdef CONFIG_TICKER
    // TICKER
    the_ticker.init(0, 8, 0, dm_width-1);
    /* sprintf_P(ticker_text, PSTR("Jam Sholat, compiled %s %s"), __DATE__, __TIME__); */
    /* the_ticker.set_text(ticker_text); */
    /* the_ticker.run(); */
#endif

    // audible
    beep_timer = 1;
}

void loop()
{// MAIN LOOP

    // SERIAL
    scmd.readSerial();

    if(ISSET(signal, SIG_APPL))
    {// reset on config change apply signal
        // clear all signal
        signal = 0;

        // reset timer
        timer = 0;

        // clear display
        dm_clear();
    }

#ifdef CONFIG_RTC
    //{ Sinkronisasi waktu dengan RTC
    static uint8_t det;

    now = rtc.now();

    if(det != now.second())
    {
        det = now.second();
        SET(signal, SIG_DETI);
        floating_time = now.hour() + now.minute()/60.0;
    }
    //}

    if(ISSET(signal, SIG_DETI))
    {// event setiap detik

        // clear SIG_DETI
        CLEAR(signal, SIG_DETI);

        if(!timer)
        {

            //{ HITUNG WAKTU SHOLAT
            sholat.hisab(
                    runtime.config.lat/1000.0,
                    runtime.config.lon/1000.0,
                    runtime.config.alt/1000.0,
                    runtime.config.tz/1000.0,
                    now.year(),
                    now.month(),
                    now.day(),
                    runtime.config.asub/1000.0,
                    runtime.config.aset/1000.0,
                    runtime.config.asya/1000.0,
                    runtime.config.asar,
                    runtime.config.hilat
                    );

            //ADJUSTMENT WAKTU SHOLAT
            sholat.waktu[SUBUH] += runtime.config.msub/60.0;
            sholat.waktu[DHUHA] += runtime.config.mdhu/60.0;
            sholat.waktu[ZHUHUR] += runtime.config.mzhu/60.0;
            sholat.waktu[ASAR] += runtime.config.masr/60.0;
            sholat.waktu[MAGHRIB] += runtime.config.mmag/60.0;
            sholat.waktu[ISYA] += runtime.config.misy/60.0;
            //}

            //{TIME TRACKING
            unsigned int s = 0;

            // get current time name
            for(uint8_t i = 0; i < JUMLAH_WAKTU; i++)
            {
                unsigned int d = fix_hour(sholat.waktu[i]-floating_time)*60;

                if(d == 0)
                {
#ifdef CONFIG_DEBUG0
                    strcpy_P(buf, PSTR("time_track(): d = 0"));
                    Serial.println(buf);
#endif
                    current_time = i;
                    break;
                }

                if(d > s)
                {
#ifdef CONFIG_DEBUG0
                    sprintf_P(buf, PSTR("time_track(): i=%d d=%d s=%d"), i, d, s);
                    Serial.println(buf);
#endif
                    current_time = i;
                    s = d;
                }
            }

            // determine next time name
            next_time = current_time + 1;

            if(next_time == JUMLAH_WAKTU)
                next_time = 0;

            // calculate seconds duration to the next time
            timer = fix_hour(sholat.waktu[next_time]-floating_time)*60;
            timer = (timer * 60) - now.second();

#ifdef CONFIG_DEBUG
            sprintf_P(buf, PSTR("time_track(): curr=%d next=%d dur=%ld --> "), current_time, next_time, timer);
            strcat_P(buf, nama_waktu[current_time]);
            Serial.println(buf);
#endif
            //}

            // NOTIFICATION & AUDIBLE
            if(ISSET(signal, SIG_TFIN))
            {
                if(SHOLAT_5_WAKTU)
                {
                    // audible 5x
                    beep_timer = 5;
#ifdef CONFIG_IQOMAH
                    // set notification
                    SET(signal, SIG_TNOT);

                    // make sure wait timer is reset
                    wait_timer = 0;
#endif
                }
                else
                {
                    beep_timer = 1;
                }
            }

            // last, clear the signal
            CLEAR(signal, SIG_TFIN);

            // clear display
            dm_clear();

            // taruh hijri disini, setelah waktu sholat ditentukan
            hijri();

        }
        else
        {
            // PERINGATAN WAKTU AKAN BERAKHIR
            if(timer < (runtime.config.tfin*60))
            {
                if(!ISSET(signal, SIG_TFIN))
                {
                    SET(signal, SIG_TFIN);

                    if(SHOLAT_5_WAKTU || SHOLAT_DHUHA)
                        beep_timer = 1;

#ifdef CONFIG_DEBUG
                    sprintf_P(buf, PSTR("%02d:%02d:%02d SIG_TFIN: Menjelang akhir waktu "), now.hour(), now.minute(), now.second());
                    strcat_P(buf, nama_waktu[current_time]);
                    Serial.println(buf);
#endif
                }
            }

            timer--;
        }

#ifdef CONFIG_IQOMAH
        /*
         * Setelah masuk waktu sholat, dilanjutkan dengan
         * iqomah dan screen blank
         * */
        if(ISSET(signal, SIG_TNOT))
        {
            if(!wait_timer)
            {
#ifdef CONFIG_DEBUG
                 sprintf_P(buf, PSTR("%02d:%02d:%02d SIG_TNOT: Telah masuk waktu "), now.hour(), now.minute(), now.second());
                 strcat_P(buf, nama_waktu[current_time]);
                 Serial.println(buf);
#endif
                wait_timer = runtime.config.tnot * 60;

                // clear screen
                dm_clear();

#ifdef CONFIG_TICKER
                strcpy_P(ticker_text, PSTR("*** Telah masuk waktu sholat "));
                strcat_P(ticker_text, nama_waktu[JUMLAH_WAKTU+current_time]);
                strcat_P(ticker_text, PSTR(" ***"));
                the_ticker.stop();
                the_ticker.reset();
                the_ticker.set_text(ticker_text);
                the_ticker.run(5);
#endif
            }
            else
            {
                wait_timer--;

                if(!wait_timer)
                {
                    CLEAR(signal, SIG_TNOT);

                    if(runtime.config.tiqo)
                        SET(signal, SIG_TIQO);
                }
            }
        }

        if(ISSET(signal, SIG_TIQO))
        {
            if(!wait_timer)
            {
                if(current_time==SUBUH) wait_timer = runtime.config.tiqs; else
                if(current_time==ZHUHUR) wait_timer = runtime.config.tiqz; else
                if(current_time==ASAR) wait_timer = runtime.config.tiqa; else
                if(current_time==MAGHRIB) wait_timer = runtime.config.tiqm; else
                if(current_time==ISYA) wait_timer = runtime.config.tiqi;

                // set to default if wait_timer is zero
                if(!wait_timer)
                    wait_timer = runtime.config.tiqo;

                wait_timer *= 60; // ubah menjadi detik

#ifdef CONFIG_DEBUG
                sprintf_P(buf, PSTR("%02d:%02d:%02d SIG_TIQO: Count down iqomah (%d detik)"), now.hour(), now.minute(), now.second(), wait_timer);
                Serial.println(buf);
#endif

                // set to default wait timer on respective pray time
                // clear screen
                dm_clear();
            }
            else
            {
                wait_timer--;

                if(!wait_timer)
                {
                    CLEAR(signal, SIG_TIQO);
                    SET(signal, SIG_TOFF);
                    beep_timer = 3;

#ifdef CONFIG_TICKER
                    the_ticker.stop();
                    the_ticker.reset();
                    dm_clear();
                    the_ticker.init(0, 3, 0, dm_width-1);
                    strcpy_P(ticker_text, PSTR("*** Luruskan dan rapatkan shof demi kesempurnaan sholat, serta matikan alat komunikasi demi kekhusyukan ibadah kita ***"));
                    the_ticker.set_text(ticker_text);
                    the_ticker.run(1);
#endif
                }
            }
        }

        if(ISSET(signal, SIG_TOFF))
        {
            if(!wait_timer)
            {
#ifdef CONFIG_DEBUG
                sprintf_P(buf, PSTR("%02d:%02d:%02d SIG_TOFF: Blank screen"), now.hour(), now.minute(), now.second());
                Serial.println(buf);
#endif

#ifdef CONFIG_TICKER
                if(!the_ticker.is_running())
                {
                    wait_timer = runtime.config.toff * 60;

                    // kembalikan ticker ke posisi default
                    the_ticker.init(0, 8, 0, dm_width-1);
                }
#else
                wait_timer = runtime.config.toff * 60;

                // clear screen
                dm_clear();
#endif
            }
            else
            {
                wait_timer--;

                if(!wait_timer)
                {
                    CLEAR(signal, SIG_TOFF);
                    dm_clear();
                    txt_adab_idx = 3;
                }
            }
        }

        // UPDATING DISPLAY
        if(!ISSET(signal, SIG_TOFF))
        {// only do when display is ON

            if(!ISSET(signal, SIG_TIQO))
#endif

            {// show clock

#ifdef CONFIG_ONE_PANEL
//{
                sprintf_P(buf, PSTR("%02d"), now.hour());
                dm_string(5, 0, buf);
                sprintf_P(buf, PSTR("%02d"), now.minute());

                dm_string(17, 0, buf);
                dm_char(15, 0, ':', GRAPHICS_XOR);
                // SHOW TIME NAME
                sprintf_P(buf, nama_waktu[JUMLAH_WAKTU+current_time]);

                uint8_t d = string_width(buf)/2;

                if((SHOLAT_5_WAKTU || current_time == DHUHA) && ISSET(signal, SIG_TFIN))
                    dm_string((dm_width/2)-d, 8, buf, GRAPHICS_XOR);
                else
                    dm_string((dm_width/2)-d, 8, buf);
#endif
//}

#ifdef CONFIG_THREE_PANEL
//{

                switch(top_content_id)
                {
                    case 'SAAT':
                        sprintf_P(buf, PSTR("%02d:%02d:%02d"), now.hour(), now.minute(). now.second());
                        break;
                    case 'HIJRI':
                        sprintf_P(buf, PSTR("%S, %02d %S %02d H"),
                            (wchar_t*)&cal_islamic_weekdays[now.dayOfTheWeek()],
                            hijriyah.d,
                            (wchar_t*)&cal_islamic_months[hijriyah.m-1],
                            hijriyah.y);
                };

                // drawing
                uint8_t start_x = string_width(buf)/2;
                dm_string(dm_width-start_x, 0, buf);

/*
                sprintf_P(buf, PSTR("%02d"), now.hour());
                dm_string(0, 0, buf);
                sprintf_P(buf, PSTR("%02d"), now.minute());
                dm_string(12, 0, buf);
                dm_char(10, 0, ':', GRAPHICS_XOR);
                // SHOW TIME NAME
                // huruf kapital
                sprintf_P(buf, nama_waktu[current_time]);

                uint8_t d = string_width(buf)-1;

                if((SHOLAT_5_WAKTU || current_time == DHUHA) && ISSET(signal, SIG_TFIN))
                    dm_string(dm_width-d, 0, buf, GRAPHICS_XOR);
                else
                    dm_string(dm_width-d, 0, buf);
*/
#endif
//}
            }

#ifdef CONFIG_IQOMAH
            else
            {// Show iqomah count down
#ifdef CONFIG_ONE_PANEL
                uint8_t m = wait_timer/60.0;
                uint8_t s = wait_timer-(m*60);

                // countdown letaknya di tengah
                sprintf_P(buf, PSTR("%02d:%02d"), m, s);
                dm_string(4, 0, buf);

                strcpy_P(buf, PSTR("Iqomah"));
                uint8_t d = string_width(buf)/2;
                dm_string((dm_width/2)-d, 8, buf);
#endif

#ifdef CONFIG_THREE_PANEL
                uint8_t m = wait_timer/60.0;
                uint8_t s = wait_timer-(m*60);

                // countdown letaknya di kanan
                sprintf_P(buf, PSTR("-%02d:%02d"), m, s);
                uint8_t w = string_width(buf) -1;
                dm_string(dm_width-w, 0, buf);

                strcpy_P(buf, PSTR("IQOMAH"));
                dm_string(0, 0, buf);
#endif

            }
        }
        else
        {
            // show blink on screen off during wait_timer
            if(wait_timer)
                dm_line((dm_width/2)-1, 7, (dm_width/2), 7, GRAPHICS_XOR);
        }
#endif

        // BEEP
        if(beep_timer)
        {
            if(is_beeping)
            {
                CLEAR(PORTC, PIN_BEEP);
                is_beeping = false;
                beep_timer--;
            }
            else
            {
                SET(PORTC, PIN_BEEP);
                is_beeping = true;
            }
        }
    }
#endif

#ifdef CONFIG_SMART_REMINDER
    // CONTENT LOGIC
    if(!ISSET(signal, SIG_TOFF))
    {
        if(!the_ticker.is_running())
        {// only select content when ticker status is not running

            if(ISSET(signal, SIG_TIQO))
            {
                if(text_id > 3)
                    text_id = 0;

                if(text_id == 0)
                    strcpy_P(ticker_text, txt_doa_azan_iqomah);

                else if(text_id == 1)
                    strcpy_P(ticker_text, txt_sholat_antara_azan_iqomah);

                else if(text_id == 2)
                    strcpy_P(ticker_text, txt_keutamaan_sholat_sunnah_rawatib);

                else if(text_id == 3)
                {
                    if(current_time == SUBUH)
                        strcpy_P(ticker_text, txt_sebelum_subuh);
                    else
                        strcpy_P(ticker_text, txt_sholat_sunnah_rawatib);
                }

                text_id++;

                the_ticker.set_text(ticker_text);
                the_ticker.run();
            }
            else
            {

                if(text_id >= TXT_COUNT)
                    text_id = 0;

                if(text_id == TXT_IDENTITAS)
                {
                    // dechiper text
                    memcpy_P(ticker_text, txt_chipered, TXT_CHIPER_LEN);
                    for(uint8_t n=0; n<TXT_CHIPER_LEN; n++)
                    {
                        uint8_t x = ticker_text[n];
                        x ^= TXT_KEY;
                        ticker_text[n] = x;
                    }
                    // kasih terminating character
                    ticker_text[TXT_CHIPER_LEN] = '\0';

                    the_ticker.set_text(ticker_text);
                    the_ticker.run();
                } else

                if(text_id == TXT_JADWAL)
                {

                    sprintf_P(ticker_text, PSTR("%S, %02d %S %d H - %02d %S %d M"),
                            (wchar_t*)&cal_islamic_weekdays[now.dayOfTheWeek()],
                            hijriyah.d,
                            (wchar_t*)&cal_islamic_months[hijriyah.m+11],
                            hijriyah.y,
                            now.day(),
                            (wchar_t*)&cal_gregorian_months[now.month()+11],
                            now.year()
                    );

                    for(uint8_t n=0; n<JUMLAH_WAKTU; n++)
                    {
                        if (n==TERBENAM||n==TENGAH_MALAM||n==QIYAMULAIL||n==IMSYAK)
                            continue;

                        char tmp[32];
                        uint8_t h = floor(sholat.waktu[n]);
                        uint8_t m = (sholat.waktu[n]-h)*60;

                        sprintf_P(tmp, PSTR("  *  %S %02d:%02d"), (wchar_t*)&nama_waktu[JUMLAH_WAKTU+n], h, m);
                        strcat(ticker_text, tmp);
                    }

                    the_ticker.set_text(ticker_text);
                    the_ticker.run();
                } else

                if(text_id == TXT_ADAB_MASJID)
                {
#ifdef CONFIG_DEBUG
                    sprintf_P(buf, PSTR("TXT_ADAB_MASJID: %d"), txt_adab_idx);
                    Serial.println(buf);
#endif
                    if(txt_adab_idx == 0)
                        strcpy_P(ticker_text, txt_shof_pertama);
                    else if(txt_adab_idx == 1)
                        strcpy_P(ticker_text, txt_segera_ke_masjid);
                    else if(txt_adab_idx == 2)
                        strcpy_P(ticker_text, txt_tahiyatul_masjid);
                    else if(txt_adab_idx == 3)
                        strcpy_P(ticker_text, txt_dilarang_mengganggu);
                    else if(txt_adab_idx == 4)
                        strcpy_P(ticker_text, txt_hatinya_di_masjid);

                    the_ticker.set_text(ticker_text);
                    the_ticker.run();

                    txt_adab_idx++;

                    if(txt_adab_idx > 4)
                        txt_adab_idx = 0;

                } else

                if(text_id == TXT_JELANG_AKHIR)
                {
                    // notifikasi akhir waktu
                    if(ISSET(signal, SIG_TFIN))
                    {
                        if(SHOLAT_5_WAKTU || SHOLAT_DHUHA)
                        {
                            strcpy_P(ticker_text, PSTR("*** Menjelang akhir waktu sholat "));
                            strcat_P(ticker_text, nama_waktu[JUMLAH_WAKTU+current_time]);
                            strcat_P(ticker_text, PSTR(" ***"));
                            the_ticker.set_text(ticker_text);
                            the_ticker.run(2);
#ifdef CONFIG_DEBUG
                            Serial.println("TXT_JELANG_AKHIR");
#endif
                        }
                    }
                } else

                if(text_id == TXT_HADITS)
                {
                    // set default text, in case ngga ada yang match
                    // this is txt_hadits_idx = 0, alias default
                    strcpy_P(ticker_text, txt_hadits_default);

                    if(txt_hadits_idx == 1)
                    {
                        if(current_time == SUBUH)
                            strcpy_P(ticker_text, txt_berdiam_setelah_subuh);

                        else if(current_time == DHUHA)
                            strcpy_P(ticker_text, txt_reminder_dhuha);

                        else
                            txt_hadits_idx++;
                    }

                    if(txt_hadits_idx == 2)
                    {
                        if(hijriyah.m == MUHARRAM)
                             strcpy_P(ticker_text, txt_puasa_muharram);

                        else if(hijriyah.m == RAMADHAN)
                             strcpy_P(ticker_text, txt_puasa_ramadhan);

                        else if(hijriyah.m == SYAWWAL)
                             strcpy_P(ticker_text, txt_puasa_syawal);

                        else if(hijriyah.m == DZULHIJJAH)
                            strcpy_P(ticker_text, txt_puasa_dzulhijjah);
                        else
                            txt_hadits_idx++;
                    }

                    if(txt_hadits_idx == 3)
                    {
                        if(!BULAN_RAMADHAN)
                        {
                            if(AYYAMULBIIDH)
                                strcpy_P(ticker_text, txt_puasa_ayyamulbiidh);
                            else if(!LAGI_PUASA)
                            {
                                if(SENEN_KAMIS)
                                    strcpy_P(ticker_text, txt_reminder_puasa_senin_kamis);
                            }
                        }
                    }

#ifdef CONFIG_DEBUG
                    sprintf_P(buf, PSTR("TXT_HADITS: %d"), txt_hadits_idx);
                    Serial.println(buf);
#endif

                    the_ticker.set_text(ticker_text);
                    the_ticker.run();

                    txt_hadits_idx++;

                    if(txt_hadits_idx > 3)
                        txt_hadits_idx = 0;

                }

                text_id++;
            }
        }
    }
#endif

#ifdef CONFIG_TICKER
    //{ TICKER ANIMATION
    if(f_ticker)
    {
        f_ticker = false;

        if(the_ticker.is_running())
            the_ticker.step();

        the_ticker.repeat();
    }
    //}
#endif

} // EOF loop()

#ifdef CONFIG_TICKER
ISR(TIMER2_OVF_vect)
{
    static uint8_t timer_ticker;

    TCNT2 = 130;

    if(!timer_ticker--)
    {
        timer_ticker = 40;
        f_ticker = true;
    }
}
#endif
