/* vim: set sw=4 ts=4 sts=4 tw=78 foldmarker={,} foldlevel=0 foldmethod=marker nospell: */

/**
 * Dot Matrix 2 Ticker -- A light weight dot matrix ticker library.
 *
 * Implements ticker/running text.
 *
 * This library inspired by DMD2 library by Freetronics.
 *
 * 2017/1438H Ardhy W. Nugroho
 *
 * */

#include "dm2.h"
#include "ticker.h"
#include <avr/eeprom.h>

void MyTicker::init(uint8_t x, uint8_t y, uint8_t l, uint8_t r)
{
    left = l;
    right = r;
    pos_x = r;
    pos_y = y;
    offset = 0;
    is_run = false;
    repeat_count = 0;
}

void MyTicker::reset()
{
    offset = 0;
    pos_x = right;
    repeat_count = 0;
}

void MyTicker::set_text(char* t)
{// set the text buffer pointer
    text = t;
    text_len = strlen(text);
}

void MyTicker::run(uint8_t n)
{
    if(n)
        repeat_count = n;

    is_run = true;
}

void MyTicker::stop()
{
    is_run = false;
}

bool MyTicker::is_running()
{
    return is_run;
}

void MyTicker::repeat()
{
    if(!is_run)
    {
        if(repeat_count > 1)
        {
            repeat_count--;
            run();
        }
    }
}

void MyTicker::step()
{// step move the ticker from right to the left

    // set view port
    dm_viewport(left, 0, right, dm_height);

    uint8_t width;
    char c;

    c = text[offset];

    width = char_width(c);

    // kalau ticker x sudah off screen
    if(pos_x-- < (left - width))
    {
        offset++;
        pos_x = left;
        //pos_x = left-1;

        if(offset == text_len)
        {
            is_run = false;
            offset = 0;
            pos_x = right;
            return;
        }
    }

    int w = 0;
    unsigned int i = offset;

    while(1)
    {
        // kalau sudah sampai di akhir string then break
        if(i == text_len)
            break;

        // kalau total w sudah melampaui view port then break
        if((w >= right + 10))
            break;

        c = text[i];

        i++;

        w += dm_char((pos_x + w), pos_y, c);

        // kasih padding
        for(uint8_t dx = 0; dx < font_padding; dx++)
        {
            for(uint8_t dy = 0; dy < font_header.height; dy++)
            {
                dm_pixel((pos_x + w + dx), pos_y+dy, GRAPHICS_OFF);
            }

            w++;
        }
    }
}
