/*
 * Matematika astronomi
 * ardhyw
 * */

#include "astro.h"

#ifdef __AVR__
#include <Arduino.h>
#else
#include <math.h>
#endif

/** Normalisasi nilai sudut dalam range 360 derajat
 *
 * @return nilai sudut yang sudah dinormalisasi dalam
 * rentang 0-359 derajat.
 * */
double fix_angle(
        double a ///< sudut
        )
{
    a = a - 360.0 * floor(a/360.0);
    a = a < 0.0 ? a + 360.0 : a;
    return a;
}

/// Normalisasi nilai waktu dalam rentang 24 jam
double fix_hour(
        double a ///< jam
        )
{
    a = a - 24.0 * floor(a/24.0);
    a = a < 0.0 ? a+24.0 : a;
    return a;
}

#ifndef __AVR__
/** Konversi derajat -> radian
 *
 * Khusus non __AVR__
 * */
static double radians(double d){ return d * M_PI / 180; }
/** Konversi radian -> degree
 *
 * Khusus non __AVR__
 * */
static double degrees(double r){ return r * 180 / M_PI; }
#endif
/// Versi degree untuk sin()
double dsin(double d){ return sin(radians(d)); }

/// Versi degree untuk cos()
double dcos(double d){ return cos(radians(d)); }

/// Versi degree untuk tan()
double dtan(double d){ return tan(radians(d)); }

/// Versi degree untuk asin()
double dasin(double r){ return degrees(asin(r)); }

/// Versi degree untuk acos()
double dacos(double r){ return degrees(acos(r)); }

/// Versi degree untuk acot()
double dacot(double r){ return degrees(atan(1.0/r)); }

/// Menghitung sudut deklinasi matahari pada suatu julian day
double as_decl(
        double jd ///< julian day number
        )
{
    /* double t = fix_angle(degrees(2 * M_PI * (jd - 2451545) / 365.25)); */
    /* double d = 0.37877 + 23.264*dsin(57.297*t - 79.547) + 0.3812*dsin(2*57.297*t - 82.682) + 0.17132*dsin(3*57.297*t - 59.722); */
    double n = jd - 2451545.0;
    double L = fix_angle(280.460 + 0.9856474*n);
    double g = fix_angle(357.528 + 0.9856003*n);
    double a = L + 1.915*dsin(g) + 0.020*dsin(2*g);
    double e = 23.439 - 0.0000004*n;
    double d = dasin(dsin(e)*dsin(a));

    return d;
}

/// Menghitung equation of time pada suatu julian day
double as_eqt(
        double jd ///< julian day number
        )
{
    double u = (jd - 2451545) / 36525.0;
    double l0 = fix_angle(280.46607 + 36000.7689*u);
    double et = -(1789 + 237*u)*dsin(l0)
                - (7146 - 62*u)*dcos(l0)
                + (9934 - 14*u)*dsin(2*l0)
                - (29 + 5*u)*dcos(2*l0)
                + (74 + 10*u)*dsin(3*l0)
                + (320 - 4*u)*dcos(3*l0)
                - 212*dsin(4*l0);
    et /= 1000.0;
    return et;
}

/// Menghitung waktu berdasarkan sudut ketinggian (altitude) matahari
double as_hour_angle(
        double jd, ///< julian day number
        double altitude, ///< sudut ketinggian matahari
        double latitude,
        char& error ///< error flag
        )
{
    double d = as_decl(jd);
    double cos_ha = (dsin(altitude) - dsin(latitude)*dsin(d)) / (dcos(latitude)*dcos(d));

    if((cos_ha > 1) || (cos_ha < -1))
    {
        error = 1;
        return -12;
    }
    else
    {
        return dacos(cos_ha)/15.0;
    }
}
