/*
 * Waktu Sholat
 * ============
 * Hisab waktu sholat
 * */

#ifndef WSHOLAT_H
#define WSHOLAT_H

// testing purpose
#include <stdint.h>

#define JUMLAH_WAKTU    11
#define IMSYAK          0
#define SUBUH           1
#define TERBIT          2
#define DHUHA           3
#define ZHUHUR          4
#define ASAR            5
#define TERBENAM        6
#define MAGHRIB         7
#define ISYA            8
#define TENGAH_MALAM    9
#define QIYAMULAIL      10

#ifndef __AVR__
const char* waktu[JUMLAH_WAKTU][13] =
{
    "Imsyak",
    "Subuh",
    "Syuruq",
    "Dhuha",
    "Zhuhur",
    "Asar",
    "Terbenam",
    "Maghrib",
    "Isya",
    "Tengah malam",
    "Qiyamul-lail"
};

#else

#include <avr/pgmspace.h>
const char nama_waktu[JUMLAH_WAKTU*2][13] PROGMEM =
{
    {"IMSYAK"},
    {"SUBUH"},
    {"SYURUQ"},
    {"DHUHA"},
    {"ZHUHUR"},
    {"ASAR"},
    {"TERBENAM"},
    {"MAGHRIB"},
    {"ISYA"},
    {"MALAM"},
    {"QIYAM-LAIL"},

    // Untuk small device
    {"Imsyak"},
    {"Subuh"},
    {"Syuruq"},
    {"Dhuha"},
    {"Zhuhur"},
    {"Asar"},
    {"Senja"},
    {"Maghrib"},
    {"Isya"},
    {"Tengah Malam"},
    {"Qiyamul-Lail"}
};
#endif

class WaktuSholat
{
    public:
        // functions
        /* WaktuSholat(); */
        void hisab(
                double latitude,
                double longitude,
                double altitude,
                double timezone,
                uint16_t tahun,
                uint8_t bulan,
                uint8_t hari,
                double subuh,
                double terbit,
                double isya,
                uint8_t asar,
                uint8_t high_latitude);

        // vars
        double waktu[JUMLAH_WAKTU];

    private:
};
#endif
