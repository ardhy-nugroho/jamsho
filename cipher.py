#!/usr/bin/env python
import sys

out_str = """
#ifndef TXT_ID
#define TXT_ID
#define TXT_KEY '{k}'
const char txt_identitas[] PROGMEM = "{text}";
#define TXT_CHIPER_LEN {l}
const char txt_chipered[] PROGMEM = {{{cph}}};
#endif
"""

if __name__ == '__main__':
    with open('id.h', 'w') as output:
        text = sys.argv[1]
        # uppss... ini harus konstan 'a' ya! 
        # karena hardcoded
        k = 'a'
        cph = ','.join(map(lambda a: str(ord(a)^ord(k)), text))
        output.write(out_str.format(k=k, text=text, cph=cph, l=len(text)))
