/**
 * @file
 * Dari http://www.fourmilab.ch/documents/calendar/?cm_mc_uid=35557280664714912353311&cm_mc_sid_50200000=1491235331
 * */

#include "calendar.h"
#include <math.h>

///  LEAP_GREGORIAN  --  Is a given year in the Gregorian calendar a leap year ?
char cal_leap_gregorian(unsigned int year)
{
    return ((year % 4) == 0) && (!(((year % 100) == 0) && ((year % 400) != 0)));
}

///  GREGORIAN_TO_JD  --  Determine Julian day number from Gregorian calendar date
double cal_gregorian_to_jd(
    unsigned int year,
    unsigned char month,
    unsigned char day
)
{
    // cara ini, terlalu berat utk AVR.
    // Hasilnya error, sedangkan kompilasi
    // versi PC-nya OK.
#ifndef __AVR__
    return (CAL_GREGORIAN_EPOCH - 1)
        + (365 * (year - 1)) + floor((year - 1) / 4) + (-floor((year - 1) / 100)) + floor((year - 1) / 400)
        + floor((((367 * month) - 362) / 12) + ((month <= 2) ? 0 : (cal_leap_gregorian(year) ? -1 : -2))
        + day);
#else
    if(month <= 2)
    {
        year -= 1;
        month += 12;
    }

    int a = (int)(year / 100);
    int b = (int)(2 - a + floor(a/4));
    return 1720994.5
        + b
        + floor(365.25 * year)
        + floor(30.6001 * (month + 1))
        + day;
#endif
}

///  JD_TO_GREGORIAN  --  Calculate Gregorian calendar date from Julian day
void cal_jd_to_gregorian(
    double jd,
    unsigned int& year,
    unsigned char& month,
    unsigned char& day
)
{
    double wjd = floor(jd - 0.5) + 0.5;
    double depoch = wjd - CAL_GREGORIAN_EPOCH;
    double quadricent = floor(depoch / 146097);
    unsigned int dqc = fmod(depoch, 146097);
    unsigned int cent = floor(dqc / 36524);
    unsigned int dcent = fmod(dqc, 36524);
    unsigned int quad = floor(dcent / 1461);
    unsigned int dquad = fmod(dcent, 1461);
    unsigned int yindex = floor(dquad / 365);
    year = (quadricent * 400) + (cent * 100) + (quad * 4) + yindex;

    if (!((cent == 4) || (yindex == 4)))
        year += 1;

    double yearday = wjd - cal_gregorian_to_jd(year, 1, 1);
    double leapadj = ((wjd < cal_gregorian_to_jd(year, 3, 1)) ? 0 : (cal_leap_gregorian(year) ? 1 : 2));
    month = floor((((yearday + leapadj) * 12) + 373) / 367);
    day = (wjd - cal_gregorian_to_jd(year, month, 1)) + 1;
}

///  ISLAMIC_TO_JD  --  Determine Julian day from Islamic date
double cal_islamic_to_jd(
    unsigned int year,
    unsigned char month,
    unsigned char day
)
{
    return (day
        + ceil(29.5 * (month - 1))
        + (year - 1) * 354UL ///< dengan hilang UL, telah menyebabkan overflow, sehingga hasil perhitungannya salah
        + floor((3 + (11 * year)) / 30)
        + CAL_ISLAMIC_EPOCH
        ) - 1;
}

///  JD_TO_ISLAMIC  --  Calculate Islamic date from Julian day
void cal_jd_to_islamic(
        double jd,
        unsigned int &year,
        unsigned char &month,
        unsigned char &day
)
{
    jd = floor(jd) + 0.5;
    year = floor(((30 * (jd - CAL_ISLAMIC_EPOCH)) + 10646) / 10631);

    double a = ceil((jd - (29 + cal_islamic_to_jd(year, 1, 1))) / 29.5) + 1;

    if(a < 12) month = a; else month = 12;

    day = (jd - cal_islamic_to_jd(year, month, 1)) + 1;
}
