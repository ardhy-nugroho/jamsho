#ifndef ASTRO_H
#define ASTRO_H

double as_decl(double jd);
double as_eqt(double jd);
double as_hour_angle(double jd, double altitude, double latitude, char& error);
// helper function
double fix_angle(double a);
double fix_hour(double a);
double dsin(double d);
double dcos(double d);
double dtan(double d);
double dacos(double r);
double dacot(double r);

#endif
