/* vim: set sw=4 ts=4 sts=4 tw=78 foldmarker={,} foldlevel=0 foldmethod=marker nospell: */

#include "wsholat.h"
#include "calendar.h"
#include "astro.h"
#include <math.h>

void WaktuSholat::hisab(
        double latitude,
        double longitude,
        double altitude,
        double timezone,
        uint16_t tahun,
        uint8_t bulan,
        uint8_t hari,
        double subuh,
        double terbit,
        double isya,
        uint8_t asar,
        uint8_t high_latitude
        )
{
    // catch error on high latitude location
    char err = 0;

    // hitung julian days
    double jd = cal_gregorian_to_jd(tahun, bulan, hari);

    uint8_t i = JUMLAH_WAKTU;

    while(i--)
    {
         waktu[i] = jd - latitude/(15*24) + waktu[i]/24.0;
    }

    double tengah_hari = 12 + timezone - longitude/15.0 - as_eqt(jd)/60.0;
    waktu[ZHUHUR] = tengah_hari + 1/60.0;

    double angle = dacot(asar + dtan(fabs(as_decl(waktu[ASAR]) - latitude)));
    waktu[ASAR] = tengah_hari + as_hour_angle(waktu[ASAR], angle, latitude, err);

    angle = terbit + 0.0347*sqrt(fabs(altitude));
    waktu[TERBIT] = tengah_hari - as_hour_angle(waktu[TERBIT], -angle, latitude, err);
    waktu[DHUHA] = waktu[TERBIT] + 30/60.0;

    waktu[TERBENAM] = tengah_hari + as_hour_angle(waktu[TERBENAM], -angle, latitude, err);
    waktu[MAGHRIB] = waktu[TERBENAM] + 1/60.0;

    angle = isya + 0.0347*sqrt(fabs(altitude));
    waktu[ISYA] = tengah_hari + as_hour_angle(waktu[ISYA], -angle, latitude, err);

    angle = subuh + 0.0347*sqrt(fabs(altitude));
    waktu[SUBUH] = tengah_hari - as_hour_angle(waktu[SUBUH], -angle, latitude, err);

    double diff = fix_hour(waktu[TERBIT] - waktu[TERBENAM]);
    waktu[TENGAH_MALAM] = fix_hour(waktu[MAGHRIB] + diff/2.0);
    waktu[QIYAMULAIL] = fix_hour(waktu[TERBIT] - diff/3.0);
    waktu[IMSYAK] = fix_hour(waktu[SUBUH] - 10.0/60.0);

}
