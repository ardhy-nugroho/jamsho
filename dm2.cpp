/* vim: set sw=4 ts=4 sts=4 tw=78 foldmarker={,} foldlevel=0 foldmethod=marker nospell: */

/**
 * Dot Matrix 2 -- A light weight dot matrix library.
 *
 * Implements basic dot matrix operation.
 *
 * This library inspired by DMD2 library by Freetronics.
 *
 * 2017/1438H Ardhy W. Nugroho
 *
 * */

#include "dm2.h"
FontHeader font_header;
int cur_x;
int cur_y;
unsigned char* font;
unsigned char scan_row;
char* bitmap;
unsigned char dm_height;
unsigned char dm_width;
unsigned char row_bytes;
char step_x;
char step_y;
unsigned char brightness = 1;
uint8_t font_padding;
int vx1, vx2, vy1, vy2;

void dm_init(unsigned char w, unsigned char h)
{// initialize dotmatrix display

    //{ setup SPI
    /// sebagai master node, SS harus di set sebagai output
    /// supaya tidak pindah jadi slave
    DDRB |= _BV(DM_PIN_SS);
    PORTB |= _BV(DM_PIN_SS);
    // SPI master
    SPCR |= _BV(MSTR);
    // SPI enable
    SPCR |= _BV(SPE);
    // MSBFIRST
    SPCR &= ~(_BV(DORD));
    // set data mode = SPI_MODE0
    SPCR = (SPCR & ~0x0c) | 0x00;
    // set clock divider = CLOCK DIV4
    SPCR = (SPCR & ~0x03) | (0x00 & 0x03);
    SPSR = (SPSR & ~0x01) | ((0x00 >> 2) & 0x01);
    //}

    //{ setup pin
    DDRB |= _BV(DM_PIN_SRCLK);
    DDRB |= _BV(DM_PIN_NOE);
    DDRB |= _BV(DM_PIN_SER);
    DDRB |= _BV(DM_PIN_RCLK);
    DDRD |= _BV(DM_PIN_A);
    DDRD |= _BV(DM_PIN_B);

    // write pin state
    PORTB &= ~_BV(DM_PIN_SRCLK);
    PORTB &= ~_BV(DM_PIN_NOE);
    PORTB &= ~_BV(DM_PIN_RCLK);
    PORTB |= _BV(DM_PIN_SER);
    PORTD &= ~_BV(DM_PIN_A);
    PORTD &= ~_BV(DM_PIN_B);
    //}

    //{ setup bitmap display buffer
    dm_width = w;
    dm_height = h;
    row_bytes = dm_width >> 3;
    bitmap = (char*) malloc(row_bytes * dm_height);
    //}

    //{ brightness control
    // PWM: Fast and correct freq.
    /* TCCR1B = _BV(WGM13)|_BV(CS10); */
    /* // enable output compare */
    /* TCCR1A = _BV(COM1A1); */
    /* // periode ditentukan = 1000us = 1ms */
    /* // periode PWM = F_CPU/2000000 * 1000us = 8000 */
    /* ICR1 = 8000; // periode PWM */
    /* OCR1A = (8000 >> 10) * 10; // dutycycle = 2... set=1000 untuk terang banget */
    //}

    //{ enable timer to schedule display scanning
    TIMSK1 = _BV(TOIE1);
    //}
}

void dm_clear(graphics_mode mode)
{// clear screen
    if (mode == GRAPHICS_INVERSE)
        memset(bitmap, 0x00, row_bytes * dm_height);
    else
        memset(bitmap, 0xff, row_bytes * dm_height);
}

void set_cursor(int x, int y)
{// set current drawing cursor
    cur_x = x;
    cur_y = y;
}

unsigned char is_on_screen(int x, int y)
{// check if cursor position is on the screen
    if (x < dm_width &&
        x >= 0 &&
        y < dm_height &&
        y >= 0)
        return 1;
    else
        return 0;
}

void dm_pixel(int x, int y, graphics_mode mode)
{// gambar pixel pada posisi cursor

    if( x <= vx2 &&
        x >= vx1 &&
        y <= vy2 &&
        y >= vy1
    )
    {
        // pixel to bitmap conversion
        unsigned char byte_idx = (y * row_bytes) + (x>>3);
        unsigned char bit = bitmask[(x & 0x07)];

        // update bitmap
        switch(mode)
        {
            case GRAPHICS_OFF:
                bitmap[byte_idx] |= bit;
                break;
            case GRAPHICS_ON:
                bitmap[byte_idx] &= ~bit;
                break;
            case GRAPHICS_INVERSE:
                break;
            case GRAPHICS_OR:
                bitmap[byte_idx] = ~(~bitmap[byte_idx] | bit);
                break;
            case GRAPHICS_NOR:
                bitmap[byte_idx] = (~bitmap[byte_idx] | bit);
                break;
            case GRAPHICS_XOR:
                bitmap[byte_idx] ^= bit;
                break;
            case GRAPHICS_NOP:
                break;
            default:;
        }
    }
}

void dm_scan()
{// transfer bitmap ke dot matrix
    unsigned char idx;

    //{ transfer SPI
    unsigned char block;
    for(block=0; block<row_bytes; block++)
    {
        idx = ((scan_row + 12) * row_bytes) + block;
        SPDR = bitmap[idx];
        while(!(SPSR & _BV(SPIF)));

        idx = ((scan_row + 8) * row_bytes) + block;
        SPDR = bitmap[idx];
        while(!(SPSR & _BV(SPIF)));

        idx = ((scan_row + 4) * row_bytes) + block;
        SPDR = bitmap[idx];
        while(!(SPSR & _BV(SPIF)));

        idx = ((scan_row + 0) * row_bytes) + block;
        SPDR = bitmap[idx];
        while(!(SPSR & _BV(SPIF)));

    }
    //}

    //{ scan row
    digitalWrite(DM_PIN_A, scan_row & 0x01);
    digitalWrite(DM_PIN_B, scan_row & 0x02);
    scan_row = (scan_row + 1)%4;
    //}

    //{ turn off display
    //PORTB &= ~_BV(DM_PIN_NOE);
    //analogWrite(9, 1);
    //}

    //{ latch to output register
    PORTB |= _BV(DM_PIN_RCLK);
    PORTB &= ~_BV(DM_PIN_RCLK);
    //}

    //{ turn on display
    //PORTB |= _BV(DM_PIN_NOE);
    analogWrite(9, brightness);
    //}
}

void dm_brightness(unsigned char b)
{
    brightness = b;
}

void dm_line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode)
{// taken from DMD2: draw line from cursor to position (x2,y2)
  int dy = y2 - y1;
  int dx = x2 - x1;
  int stepx, stepy;

  if (dy < 0) {
    dy = -dy;
    stepy = -1;
  } else {
    stepy = 1;
  }
  if (dx < 0) {
    dx = -dx;
    stepx = -1;
  } else {
    stepx = 1;
  }
  dy = dy * 2;
  dx = dx * 2;

  dm_pixel(x1, y1, mode);
  if (dx > dy) {
    int fraction = dy - (dx / 2);	// same as 2*dy - dx
    while (x1 != x2) {
      if (fraction >= 0) {
        y1 += stepy;
        fraction -= dx;	// same as fraction -= 2*dx
      }
      x1 += stepx;
      fraction += dy;	// same as fraction -= 2*dy
      dm_pixel(x1, y1, mode);
    }
  } else {
    int fraction = dx - (dy / 2);
    while (y1 != y2) {
      if (fraction >= 0) {
        x1 += stepx;
        fraction -= dy;
      }
      y1 += stepy;
      fraction += dx;
      dm_pixel(x1, y1, mode);
    }
  }
}

void dm_box(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode)
{// draw basic box

    dm_line(x1, y1, x2, y1, mode);
    dm_line(x2, y1, x2, y2, mode);
    dm_line(x2, y2, x1, y2, mode);
    dm_line(x1, y2, x1, y1, mode);
}

void dm_fbox(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, graphics_mode mode)
{// draw filled box
    for (unsigned int b = x1; b <= x2; b++) {
        dm_line(b, y1, b, y2, mode);
    }
}

void select_font(const unsigned char* f)
{// memilih dan loading font data
    font = (unsigned char*)f;
    memcpy_P(&font_header, (void*)font, sizeof(FontHeader));

    if(!font_header.size)
        font_padding = 1;
    else
        font_padding = 2;
}

uint8_t char_width(char letter)
{// taken from DMD2

    if(font_header.size == 0)
        // zero length is flag indicating fixed width font (array does not contain width data entries)
        // if the letter is a space then return the font's fixedWidth
        // (set as the 'width' field in New Font dialog in GLCDCreator.)
        return font_header.fixed_width;

    uint8_t _w = pgm_read_byte(font + sizeof(FontHeader) + letter - font_header.first_char);

    if(!_w)
        return 5;
    else
        return _w;

}

uint8_t string_width(const char* s)
{// return width of string "s"
    uint8_t sw = strlen(s);
    uint8_t w = 0;

    for(uint8_t i = 0; i < sw; i++)
    {
        char c = s[i];
        w += char_width(c);
    }

    return w + sw;
}

uint8_t dm_char(int x, int y, char c, graphics_mode mode)
{// menggambar huruf pada posisi kursor

    bool inverse = false;
    if(mode == GRAPHICS_INVERSE)
        inverse = true;

    uint8_t width = 0;

    if (c == ' ')
    {// jika huruf adalah spasi
        width = char_width(' ');

        if(!inverse)
            dm_fbox(x, y, x + width, y + font_header.height, GRAPHICS_OFF);
        else
            dm_fbox(x, y, x + width, y + font_header.height, GRAPHICS_ON);

        return width;
    }

    uint8_t bytes = (font_header.height + 7) / 8;
    uint16_t index = 0;

    // bisa kita disable dulu
    // karena kayaknya kurang mungkin terjadi
    /* if (c < font_header.first_char || c >= (font_header.first_char + font_header.char_count)) */
    /*     return 0; */

    c -= font_header.first_char;

    if (font_header.size == 0)
    {// fixed width font
        // zero length is flag indicating fixed width font (array does not contain width data entries)
        width = font_header.fixed_width;
        index = sizeof(FontHeader) + c * bytes * width;
    }
    else
    {// variable width font
        // variable width font, read width data, to get the index
        for (uint8_t i = 0; i < c; i++)
            index += pgm_read_byte(font + sizeof(FontHeader) + i);

        index = index * bytes + font_header.char_count + sizeof(FontHeader);
        width = pgm_read_byte(font + sizeof(FontHeader) + c);
    }

    if (x < -width || y < -font_header.height)
    {// skip if off-screen
        return width ;
    }

    // last but not least, draw the character
    //uint8_t j = width;
    uint8_t k;

    //while(j--)
    for (uint8_t j = 0; j < width; j++)
    { // Width
        for (uint8_t i = bytes - 1; i < 254; i--)
        { // Vertical Bytes
            uint8_t data = pgm_read_byte(font + index + j + (i * width));
            int offset = (i * 8);

            if ((i == bytes - 1) && bytes > 1)
                offset = font_header.height - 8;

            k = 8;
            while(k--)
            {
                if(data&(1<<k))
                    dm_pixel(x+j, y+k+offset, mode);
                else
                {
                    if(mode == GRAPHICS_NOP)
                        dm_pixel(x+j, y+k+offset, GRAPHICS_NOP);
                    else
                        dm_pixel(x+j, y+k+offset, GRAPHICS_OFF);
                }
            }

            // ini kode dari DMD2
            /* for (uint8_t k = 0; k < 8; k++) */
            /* { // Vertical bits */
            /*     if ((offset+k >= i*8) && (offset+k <= font_header.height)) */
            /*     { */
            /*         if (data & (1 << k)) */
            /*         { */
            /*             if(inverse) */
            /*                 dm_pixel(x + j, y + offset + k, GRAPHICS_OFF); */
            /*             else */
            /*                 dm_pixel(x + j, y + offset + k); */
            /*         } */
            /*         else */
            /*         { */
            /*             if(inverse) */
            /*                 dm_pixel(x + j, y + offset + k); */
            /*             else */
            /*                 dm_pixel(x + j, y + offset + k, GRAPHICS_OFF); */
            /*         } */
            /*     } */
            /* } */
        }
    }

    /* j = font_padding; */

    /* while(j--) */
    /* { */
    /*     k = font_header.height; */
    /*     while(k--) */
    /*         dm_pixel(x+width+j, y+k, GRAPHICS_OFF); */
    /* } */

    return width;
}

uint8_t dm_string(int x, int y, const char* s, graphics_mode mode)
{// menggambar string pada posisi (x,y)
    unsigned int len = strlen(s);
    int w = 0;

    for(unsigned int i = 0; i < len; i++)
    {
        // ambil karakter
        char c = s[i];

        // gambar huruf
        w += dm_char(x + w, y, c, mode);

        // kasih padding right
        //if(c != ' ')
        {
            /* if(mode == GRAPHICS_INVERSE) */
            /*     dm_line(x+w, y, x+w, y+font_header.height-1); */
            /* else */
            /*     dm_line(x+w, y, x+w, y+font_header.height-1, GRAPHICS_OFF); */

            uint8_t dy = font_header.height;

            while(dy--)
            {
                if(mode == GRAPHICS_NOP)
                    dm_pixel(x+w, y+dy, GRAPHICS_NOP);
                else
                    dm_pixel(x+w, y+dy, GRAPHICS_OFF);

            }

            // kasih spasi antar huruf
            w++;
        }
    }

    return w;
}

void dm_viewport(int x1, int y1, int x2, int y2)
{// memberikan batasan dm_pixel()
    vx1 = x1;
    vy1 = y1;
    vx2 = x2;
    vy2 = y2;

    if(!vx2)
        vx2 = dm_width-1;
    if(!vy2)
        vy2 = dm_height-1;
}

//{ ISR TIMER1
ISR(TIMER1_OVF_vect)
{
    dm_scan();
}
//}

